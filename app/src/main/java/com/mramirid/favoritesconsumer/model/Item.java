package com.mramirid.favoritesconsumer.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

    private int id;
    private String itemType, poster, name, genres, description, year, language;
    private float rating;

    public Item(int id, String itemType, String poster, String name, String genres, String description, String year, String language, float rating) {
        this.id = id;
        this.itemType = itemType;
        this.poster = poster;
        this.name = name;
        this.genres = genres;
        this.description = description;
        this.year = year;
        this.language = language;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public String getItemType() {
        return itemType;
    }

    public String getPoster() {
        return poster;
    }

    public String getName() {
        return name;
    }

    public String getGenres() {
        return genres;
    }

    public String getDescription() {
        return description;
    }

    public float getRating() {
        return rating;
    }

    public String getYear() {
        return year;
    }

    public String getLanguage() {
        return language;
    }

    private Item(Parcel in) {
        id = in.readInt();
        itemType = in.readString();
        poster = in.readString();
        name = in.readString();
        genres = in.readString();
        description = in.readString();
        rating = in.readFloat();
        year = in.readString();
        language = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(itemType);
        parcel.writeString(poster);
        parcel.writeString(name);
        parcel.writeString(genres);
        parcel.writeString(description);
        parcel.writeFloat(rating);
        parcel.writeString(year);
        parcel.writeString(language);
    }
}
