package com.mramirid.favoritesconsumer.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;
import com.mramirid.favoritesconsumer.R;
import com.mramirid.favoritesconsumer.adapter.ViewPagerAdapter;
import com.mramirid.favoritesconsumer.fragment.FavoritesFragment;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

	public static final String MOVIES = "movie";
	public static final String TV_SHOWS = "tv";

	private ViewPager viewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.favorites);

		viewPager = findViewById(R.id.viewpager);
		setupViewPager(viewPager);

		TabLayout tabLayout = findViewById(R.id.tabs);
		tabLayout.setupWithViewPager(viewPager);
	}

	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
		adapter.addFragment(new FavoritesFragment(MOVIES), getString(R.string.movies));
		adapter.addFragment(new FavoritesFragment(TV_SHOWS), getString(R.string.tv_shows));
		viewPager.setAdapter(adapter);
		viewPager.setOffscreenPageLimit(2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		if (item.getItemId() == R.id.action_refresh)
			Objects.requireNonNull(viewPager.getAdapter()).notifyDataSetChanged();
		return super.onOptionsItemSelected(item);
	}
}
