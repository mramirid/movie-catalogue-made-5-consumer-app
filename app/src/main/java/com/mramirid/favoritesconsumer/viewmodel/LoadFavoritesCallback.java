package com.mramirid.favoritesconsumer.viewmodel;

import android.database.Cursor;

public interface LoadFavoritesCallback {
	void postExecute(Cursor cursorFavorites);
}
