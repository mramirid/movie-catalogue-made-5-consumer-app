# Movie Catalogue - Submission 5 (Consumer App)

## Apa ini?

Aplikasi ini merupakan aplikasi kedua yang harus dikerjakan untuk memenuhi task Final Project MADE Dicoding

<img src="https://i.ibb.co/c3hGKxy/Screenshot-2020-12-05-011334.png" alt="Screenshot Task" border="0">

## Demo

Silahkan download demo apk nya di [sini](https://github.com/mramirid/Movie-Catalogue-MADE/releases/download/v5.0/Consumer.App.apk)